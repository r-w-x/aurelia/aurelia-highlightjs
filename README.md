# aurelia-hightlightjs

This is an [Aurelia](http://aurelia.io/) custom element for [highlight.js](https://highlightjs.org/)

## Documentation

If you are looking for documentation on how to use this custom elment, go to 
http://aurelia-highlightjs.r-w-x.net/

## Developers

First you need to bundle the lib :

``npm run bundle``

Go to site folder :

``cd ./site``

Start development bundle watcher and server :

``npm start``

