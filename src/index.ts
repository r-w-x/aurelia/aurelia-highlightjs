import {FrameworkConfiguration} from 'aurelia-framework';
import {ConfigBuilder} from './config-builder';
import './hljs.html!text'

export function configure(aurelia: FrameworkConfiguration, configCallback?: (builder: ConfigBuilder) => void) {
    aurelia.globalResources(['./hljs']);
    const builder: ConfigBuilder = aurelia.container.get(ConfigBuilder);
    builder.setDefaultTheme();
    if (configCallback && configCallback instanceof Function) {
        configCallback(builder);
    } else {
        builder.useAllLanguages();
    }
}


export * from './hljs';
