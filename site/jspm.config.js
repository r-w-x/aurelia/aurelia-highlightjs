SystemJS.config({
  nodeConfig: {
    "paths": {
      "github:": "jspm_packages/github/",
      "npm:": "jspm_packages/npm/",
      "app/": "src/"
    }
  },
  devConfig: {
    "map": {
      "plugin-typescript": "github:frankwallis/plugin-typescript@9.0.0",
      "typescript": "npm:typescript@3.1.1",
      "os": "npm:jspm-nodelibs-os@0.2.2",
      "net": "npm:jspm-nodelibs-net@0.2.1",
      "crypto": "npm:jspm-nodelibs-crypto@0.2.1",
      "vm": "npm:jspm-nodelibs-vm@0.2.1",
      "util": "npm:jspm-nodelibs-util@0.2.2",
      "stream": "npm:jspm-nodelibs-stream@0.2.1",
      "constants": "npm:jspm-nodelibs-constants@0.2.1",
      "assert": "npm:jspm-nodelibs-assert@0.2.1",
      "string_decoder": "npm:jspm-nodelibs-string_decoder@0.2.2",
      "events": "npm:jspm-nodelibs-events@0.2.2",
      "readline": "npm:jspm-nodelibs-readline@0.2.1",
      "child_process": "npm:jspm-nodelibs-child_process@0.2.1"
    },
    "packages": {
      "npm:jspm-nodelibs-os@0.2.2": {
        "map": {
          "os-browserify": "npm:os-browserify@0.3.0"
        }
      },
      "npm:jspm-nodelibs-crypto@0.2.1": {
        "map": {
          "crypto-browserify": "npm:crypto-browserify@3.12.0"
        }
      },
      "npm:crypto-browserify@3.12.0": {
        "map": {
          "browserify-cipher": "npm:browserify-cipher@1.0.1",
          "inherits": "npm:inherits@2.0.3",
          "create-ecdh": "npm:create-ecdh@4.0.3",
          "randomfill": "npm:randomfill@1.0.4",
          "browserify-sign": "npm:browserify-sign@4.0.4",
          "create-hmac": "npm:create-hmac@1.1.7",
          "public-encrypt": "npm:public-encrypt@4.0.2",
          "pbkdf2": "npm:pbkdf2@3.0.17",
          "diffie-hellman": "npm:diffie-hellman@5.0.3",
          "create-hash": "npm:create-hash@1.2.0",
          "randombytes": "npm:randombytes@2.0.6"
        }
      },
      "npm:browserify-sign@4.0.4": {
        "map": {
          "create-hmac": "npm:create-hmac@1.1.7",
          "inherits": "npm:inherits@2.0.3",
          "browserify-rsa": "npm:browserify-rsa@4.0.1",
          "bn.js": "npm:bn.js@4.11.8",
          "parse-asn1": "npm:parse-asn1@5.1.1",
          "elliptic": "npm:elliptic@6.4.1",
          "create-hash": "npm:create-hash@1.2.0"
        }
      },
      "npm:create-hmac@1.1.7": {
        "map": {
          "inherits": "npm:inherits@2.0.3",
          "safe-buffer": "npm:safe-buffer@5.1.2",
          "ripemd160": "npm:ripemd160@2.0.2",
          "cipher-base": "npm:cipher-base@1.0.4",
          "create-hash": "npm:create-hash@1.2.0",
          "sha.js": "npm:sha.js@2.4.11"
        }
      },
      "npm:browserify-cipher@1.0.1": {
        "map": {
          "browserify-des": "npm:browserify-des@1.0.2",
          "evp_bytestokey": "npm:evp_bytestokey@1.0.3",
          "browserify-aes": "npm:browserify-aes@1.2.0"
        }
      },
      "npm:pbkdf2@3.0.17": {
        "map": {
          "create-hmac": "npm:create-hmac@1.1.7",
          "safe-buffer": "npm:safe-buffer@5.1.2",
          "ripemd160": "npm:ripemd160@2.0.2",
          "create-hash": "npm:create-hash@1.2.0",
          "sha.js": "npm:sha.js@2.4.11"
        }
      },
      "npm:randomfill@1.0.4": {
        "map": {
          "safe-buffer": "npm:safe-buffer@5.1.2",
          "randombytes": "npm:randombytes@2.0.6"
        }
      },
      "npm:evp_bytestokey@1.0.3": {
        "map": {
          "safe-buffer": "npm:safe-buffer@5.1.2",
          "md5.js": "npm:md5.js@1.3.4"
        }
      },
      "npm:browserify-des@1.0.2": {
        "map": {
          "safe-buffer": "npm:safe-buffer@5.1.2",
          "inherits": "npm:inherits@2.0.3",
          "cipher-base": "npm:cipher-base@1.0.4",
          "des.js": "npm:des.js@1.0.0"
        }
      },
      "npm:public-encrypt@4.0.2": {
        "map": {
          "browserify-rsa": "npm:browserify-rsa@4.0.1",
          "bn.js": "npm:bn.js@4.11.8",
          "parse-asn1": "npm:parse-asn1@5.1.1",
          "create-hash": "npm:create-hash@1.2.0",
          "randombytes": "npm:randombytes@2.0.6"
        }
      },
      "npm:create-ecdh@4.0.3": {
        "map": {
          "bn.js": "npm:bn.js@4.11.8",
          "elliptic": "npm:elliptic@6.4.1"
        }
      },
      "npm:diffie-hellman@5.0.3": {
        "map": {
          "bn.js": "npm:bn.js@4.11.8",
          "miller-rabin": "npm:miller-rabin@4.0.1",
          "randombytes": "npm:randombytes@2.0.6"
        }
      },
      "npm:browserify-rsa@4.0.1": {
        "map": {
          "bn.js": "npm:bn.js@4.11.8",
          "randombytes": "npm:randombytes@2.0.6"
        }
      },
      "npm:parse-asn1@5.1.1": {
        "map": {
          "evp_bytestokey": "npm:evp_bytestokey@1.0.3",
          "pbkdf2": "npm:pbkdf2@3.0.17",
          "create-hash": "npm:create-hash@1.2.0",
          "asn1.js": "npm:asn1.js@4.10.1",
          "browserify-aes": "npm:browserify-aes@1.2.0"
        }
      },
      "npm:ripemd160@2.0.2": {
        "map": {
          "inherits": "npm:inherits@2.0.3",
          "hash-base": "npm:hash-base@3.0.4"
        }
      },
      "npm:cipher-base@1.0.4": {
        "map": {
          "inherits": "npm:inherits@2.0.3",
          "safe-buffer": "npm:safe-buffer@5.1.2"
        }
      },
      "npm:miller-rabin@4.0.1": {
        "map": {
          "bn.js": "npm:bn.js@4.11.8",
          "brorand": "npm:brorand@1.1.0"
        }
      },
      "npm:elliptic@6.4.1": {
        "map": {
          "bn.js": "npm:bn.js@4.11.8",
          "inherits": "npm:inherits@2.0.3",
          "hmac-drbg": "npm:hmac-drbg@1.0.1",
          "brorand": "npm:brorand@1.1.0",
          "hash.js": "npm:hash.js@1.1.5",
          "minimalistic-crypto-utils": "npm:minimalistic-crypto-utils@1.0.1",
          "minimalistic-assert": "npm:minimalistic-assert@1.0.1"
        }
      },
      "npm:md5.js@1.3.4": {
        "map": {
          "inherits": "npm:inherits@2.0.3",
          "hash-base": "npm:hash-base@3.0.4"
        }
      },
      "npm:jspm-nodelibs-stream@0.2.1": {
        "map": {
          "stream-browserify": "npm:stream-browserify@2.0.1"
        }
      },
      "npm:stream-browserify@2.0.1": {
        "map": {
          "inherits": "npm:inherits@2.0.3",
          "readable-stream": "npm:readable-stream@2.3.6"
        }
      },
      "npm:create-hash@1.2.0": {
        "map": {
          "cipher-base": "npm:cipher-base@1.0.4",
          "inherits": "npm:inherits@2.0.3",
          "md5.js": "npm:md5.js@1.3.4",
          "ripemd160": "npm:ripemd160@2.0.2",
          "sha.js": "npm:sha.js@2.4.11"
        }
      },
      "npm:hash-base@3.0.4": {
        "map": {
          "inherits": "npm:inherits@2.0.3",
          "safe-buffer": "npm:safe-buffer@5.1.2"
        }
      },
      "npm:randombytes@2.0.6": {
        "map": {
          "safe-buffer": "npm:safe-buffer@5.1.2"
        }
      },
      "npm:asn1.js@4.10.1": {
        "map": {
          "bn.js": "npm:bn.js@4.11.8",
          "inherits": "npm:inherits@2.0.3",
          "minimalistic-assert": "npm:minimalistic-assert@1.0.1"
        }
      },
      "npm:hmac-drbg@1.0.1": {
        "map": {
          "hash.js": "npm:hash.js@1.1.5",
          "minimalistic-crypto-utils": "npm:minimalistic-crypto-utils@1.0.1",
          "minimalistic-assert": "npm:minimalistic-assert@1.0.1"
        }
      },
      "npm:hash.js@1.1.5": {
        "map": {
          "inherits": "npm:inherits@2.0.3",
          "minimalistic-assert": "npm:minimalistic-assert@1.0.1"
        }
      },
      "npm:browserify-aes@1.2.0": {
        "map": {
          "cipher-base": "npm:cipher-base@1.0.4",
          "create-hash": "npm:create-hash@1.2.0",
          "evp_bytestokey": "npm:evp_bytestokey@1.0.3",
          "inherits": "npm:inherits@2.0.3",
          "safe-buffer": "npm:safe-buffer@5.1.2",
          "buffer-xor": "npm:buffer-xor@1.0.3"
        }
      },
      "npm:readable-stream@2.3.6": {
        "map": {
          "inherits": "npm:inherits@2.0.3",
          "safe-buffer": "npm:safe-buffer@5.1.2",
          "string_decoder": "npm:string_decoder@1.1.1",
          "core-util-is": "npm:core-util-is@1.0.2",
          "process-nextick-args": "npm:process-nextick-args@2.0.0",
          "isarray": "npm:isarray@1.0.0",
          "util-deprecate": "npm:util-deprecate@1.0.2"
        }
      },
      "npm:jspm-nodelibs-string_decoder@0.2.2": {
        "map": {
          "string_decoder": "npm:string_decoder@0.10.31"
        }
      },
      "npm:string_decoder@1.1.1": {
        "map": {
          "safe-buffer": "npm:safe-buffer@5.1.2"
        }
      },
      "npm:sha.js@2.4.11": {
        "map": {
          "inherits": "npm:inherits@2.0.3",
          "safe-buffer": "npm:safe-buffer@5.1.2"
        }
      },
      "npm:des.js@1.0.0": {
        "map": {
          "inherits": "npm:inherits@2.0.3",
          "minimalistic-assert": "npm:minimalistic-assert@1.0.1"
        }
      }
    }
  },
  transpiler: "plugin-typescript",
  packages: {
    "app": {
      "main": "app.ts",
      "format": "esm",
      "defaultExtension": "ts",
      "meta": {
        "*.ts": {
          "loader": "plugin-typescript"
        }
      }
    }
  }
});

SystemJS.config({
  packageConfigPaths: [
    "github:*/*.json",
    "npm:@*/*.json",
    "npm:*.json"
  ],
  map: {
    "aurelia-binding": "npm:aurelia-binding@2.1.7",
    "aurelia-bootstrapper": "npm:aurelia-bootstrapper@2.3.0",
    "aurelia-dependency-injection": "npm:aurelia-dependency-injection@1.4.2",
    "aurelia-event-aggregator": "npm:aurelia-event-aggregator@1.0.1",
    "aurelia-framework": "npm:aurelia-framework@1.3.0",
    "aurelia-highlightjs": "npm:aurelia-highlightjs@0.1.10",
    "aurelia-history": "npm:aurelia-history@1.1.0",
    "aurelia-history-browser": "npm:aurelia-history-browser@1.2.0",
    "aurelia-loader": "npm:aurelia-loader@1.0.0",
    "aurelia-loader-default": "npm:aurelia-loader-default@1.0.4",
    "aurelia-logging": "npm:aurelia-logging@1.5.0",
    "aurelia-logging-console": "npm:aurelia-logging-console@1.0.0",
    "aurelia-metadata": "npm:aurelia-metadata@1.0.4",
    "aurelia-pal": "npm:aurelia-pal@1.8.0",
    "aurelia-pal-browser": "npm:aurelia-pal-browser@1.8.0",
    "aurelia-path": "npm:aurelia-path@1.1.1",
    "aurelia-polyfills": "npm:aurelia-polyfills@1.3.0",
    "aurelia-route-recognizer": "npm:aurelia-route-recognizer@1.2.0",
    "aurelia-router": "npm:aurelia-router@1.6.3",
    "aurelia-task-queue": "npm:aurelia-task-queue@1.3.1",
    "aurelia-templating": "npm:aurelia-templating@1.10.1",
    "aurelia-templating-binding": "npm:aurelia-templating-binding@1.4.3",
    "aurelia-templating-resources": "npm:aurelia-templating-resources@1.7.1",
    "aurelia-templating-router": "npm:aurelia-templating-router@1.3.3",
    "buffer": "npm:jspm-nodelibs-buffer@0.2.3",
    "fs": "npm:jspm-nodelibs-fs@0.2.1",
    "highlight.js": "npm:highlight.js@9.13.1",
    "module": "npm:jspm-nodelibs-module@0.2.1",
    "path": "npm:jspm-nodelibs-path@0.2.3",
    "process": "npm:jspm-nodelibs-process@0.2.1",
    "source-map-support": "npm:source-map-support@0.5.9",
    "text": "github:systemjs/plugin-text@0.0.7"
  },
  packages: {
    "npm:source-map-support@0.5.9": {
      "map": {
        "source-map": "npm:source-map@0.6.1",
        "buffer-from": "npm:buffer-from@1.1.1"
      }
    },
    "npm:jspm-nodelibs-buffer@0.2.3": {
      "map": {
        "buffer": "npm:buffer@5.2.1"
      }
    },
    "npm:buffer@5.2.1": {
      "map": {
        "ieee754": "npm:ieee754@1.1.12",
        "base64-js": "npm:base64-js@1.3.0"
      }
    },
    "npm:aurelia-fetch-client@1.7.0": {
      "map": {
        "aurelia-pal": "npm:aurelia-pal@1.8.0"
      }
    },
    "npm:aurelia-binding@2.1.7": {
      "map": {
        "aurelia-logging": "npm:aurelia-logging@1.5.0",
        "aurelia-pal": "npm:aurelia-pal@1.8.0",
        "aurelia-task-queue": "npm:aurelia-task-queue@1.3.1",
        "aurelia-metadata": "npm:aurelia-metadata@1.0.4"
      }
    },
    "npm:aurelia-highlightjs@0.1.10": {
      "map": {
        "aurelia-fetch-client": "npm:aurelia-fetch-client@1.7.0",
        "aurelia-templating": "npm:aurelia-templating@1.10.1",
        "aurelia-framework": "npm:aurelia-framework@1.3.0",
        "aurelia-polyfills": "npm:aurelia-polyfills@1.3.0"
      }
    }
  }
});
