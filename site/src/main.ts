import {Aurelia} from 'aurelia-framework';


export function configure(aurelia: Aurelia): void {
  aurelia.use.standardConfiguration()
    .plugin("aurelia-highlightjs", config => config
            .useLanguage("ruby")
            .useLanguage("java")
            .useLanguage("sql")
            .useLanguage("css")
            .useLanguage("go")
            .useLanguage("ini")
            .useLanguage("bash")
            .useLanguage("python")
            .useLanguage("javascript")
            .useLanguage("xml"));
  
  if( ! SystemJS.production ) {
    // configuration for development mode
    aurelia.use.developmentLogging();
  }

  aurelia.start().then(() => aurelia.setRoot());
}

